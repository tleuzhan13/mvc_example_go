package services

import (
	"gitlab.com/tleuzhan13/mvc_example_go/domain"
	"gitlab.com/tleuzhan13/mvc_example_go/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
