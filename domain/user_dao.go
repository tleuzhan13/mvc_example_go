package domain

import (
	"fmt"
	"gitlab.com/tleuzhan13/mvc_example_go/utils"
	"net/http"
)

var users = map[int64]*User{
	123: {
		Id:        123,
		FirstName: "Tleuzhan",
		LastName:  "Mukatayev",
		Email:     "tleuzhan13@gmail.com",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) {
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: fmt.Sprintf("user with id: %v not found", userId),
		Status:  http.StatusNotFound,
		Code:    "not_found",
	}
}
